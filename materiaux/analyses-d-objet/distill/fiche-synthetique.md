# Fiche synthétique de l'analyse de la revue en ligne Distill

## Auteurs
Sur une initiative de l'équipe de Google Brain, avec la collaboration de plusieurs universités et de chercheurs.

## Esthétique
Distill répond à une question de représentation dynamique de données, donc il y a une dimension esthétique dans la démarche.

Par ailleurs Distill offre un rendu graphique de grande qualité, permettant une lecture confortable sur plusieurs supports différents.

## Fabrication
Les articles sont rédigés directement en HTML, semble-t-il, et versionnés avec Git et plus particulièrement la plateforme GitHub.

## Fonctionnel
La revue répond à plusieurs objectifs :

- "un médium moderne pour présenter la recherche" : sortir du schéma traditionnel à base de PDF et intégrer des schémas interactifs pour illustrer des travaux de recherche de façon plus efficace. Il s'agit d'un objectif formel ;
- rendre plus transparent les recherches et ses résultats dans le domaine du _machine learning_ ;
- rendre lisible la recherche en général ;
- réduire la _dette_ de la recherche en rendant lisible et compréhensible les présentations de la recherche.

## Usage
Distill n'est pas à proprement parlé un modèle duplicable, il s'agit plutôt d'une initiative dont pourrait s'inspirer d'autres revues académiques.

## Communication
Quelques articles sur Distill :

- [Distill: Is This What Journals Should Look Like?](https://blog.dshr.org/2017/05/distill-is-this-what-journals-should.html) par David Rosenthal
- [Distill: a modern machine learning journal](https://news.ycombinator.com/item?id=13915808) sur ycombinator
- [Distill: A Journal with Interactive Images for Machine Learning Research](https://www.enago.com/academy/distill-a-journal-with-interactive-images-for-machine-learning-research/) sur Enago Academy
- [Distill - Academic machine learning journal for the 21st century](https://logfile.ch/machine-learning/research/2017/03/20/distill-academic-machine-learning-journal/)
- [Distill: Supporting Clarity in Machine Learning ](https://research.googleblog.com/2017/03/distill-supporting-clarity-in-machine.html)
- [What is Distill, and what's its significance to Machine Learning research?](https://www.quora.com/What-is-Distill-and-whats-its-significance-to-Machine-Learning-research)
- [Distill: An Interactive, Visual Journal for Machine Learning Research](https://blog.ycombinator.com/distill-an-interactive-visual-journal-for-machine-learning-research/)

## Commercialisation
Pas d'objectif de commercialisation.

Il faut noter que le principe de la revue est de publier les sources sur la plateforme GitHub, ce qui permet à n'importe qui de voir les différentes modifications, voir

## Ouvertures critiques
Cette revue numérique, Distill, pose un certain nombre de questions, voir de problèmes :

- les articles pouvant évoluer en continu, comment les citer ?
- comment archiver ces articles (voir ) ?
- qui peut (est capable de) contribuer avec les contraintes du format HTML, des formats liés à la visualisation, et Git ?
