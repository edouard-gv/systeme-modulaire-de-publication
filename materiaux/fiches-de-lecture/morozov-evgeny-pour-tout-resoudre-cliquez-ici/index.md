# Fiche de lecture : Morozov, Evgeny, Pour tout résoudre cliquez ici

## Notice bibliographique de l'ouvrage étudié

## Plan de cette fiche
- [Notes](notes.md)
- [Fiche synthétique](fiche-synthetique.md)
- [Commentaire de texte](commentaire-de-texte.md)

## Ressources
