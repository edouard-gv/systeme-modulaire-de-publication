---
title: "Introduction"
category: 3. Un système modulaire
order: 1
redirect_from:
  - /3-un-systeme-modulaire/
partiep_link: /2-experimentations/2-3-distill/
parties_link: /3-un-systeme-modulaire/3-1-les-etapes-d-une-chaine-de-publication/
repo: _memoire/3-un-systeme-modulaire/-3-0-introduction.md
---
{% include_relative -3-0-introduction.md %}
