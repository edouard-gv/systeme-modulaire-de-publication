---
title: "3.2. Les principes d'un nouveau modèle"
category: 3. Un système modulaire
order: 3
partiep_link: /3-un-systeme-modulaire/3-1-les-etapes-d-une-chaine-de-publication/
parties_link: /5-conclusion/
repo: _memoire/3-un-systeme-modulaire/-3-2-les-principes-d-un-nouveau-modele-de-publication.md
---
{% include_relative -3-2-les-principes-d-un-nouveau-modele-de-publication.md %}
